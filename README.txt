Search API Solr Boost By Date

Search API Solr doesn't provide a way to set boosting the results by date from the UI. Instead, you have to alter the query programmatically (https://www.drupal.org/project/search_api_solr/issues/2855329).

This module provides a "Boost by Date" views filter to Search API index-based views that can be used to achieve the boosting without programming.

Usage:
1. create a Search API solr server and index.
2. set at least one "Date" field in the index. For example, the node's created field can be used.
3. create a Search API index view.
4. set descending order by relevance.
5. add "Boost by Date" filter.
6. Optional: you can tweak the parameters for calculating the boost. You can read information about these parameters here: https://www.hashbangcode.com/article/drupal-8-date-search-boosting-search-api-and-solr-search

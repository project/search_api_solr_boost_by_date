<?php

namespace Drupal\search_api_solr_boost_by_date\Plugin\views\filter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\Entity\Index;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Solr boost by date.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("solr_boost_by_date")
 */
class SolrBoostByDate extends FilterPluginBase {

  public function adminSummary() {}

  protected function operatorForm(&$form, FormStateInterface $form_state) {}

  public function canExpose() {
    return FALSE;
  }

  public function query() {
    $a = Xss::filterAdmin($this->options['a_value']);
    $b = Xss::filterAdmin($this->options['b_value']);
    $m = Xss::filterAdmin($this->options['m_value']);
    $min = Xss::filterAdmin($this->options['min_value']);

    $date_field = Xss::filterAdmin($this->options['fieldname']);
    $index = $this->query->getIndex();
    $fields = $index->getServerInstance()->getBackend()->getSolrFieldNames($index);
    $solr_field = !empty($fields[$date_field]) ? $fields[$date_field] : '';

    if ($solr_field) {

      if (isset($min) && $min !== '') {
        $this->query->setOption(
          'solr_param_bf',
          "max(recip(abs(ms(NOW,{$solr_field})),$m,{$a},{$b}),{$min})"
        );
      }
      else {
        $this->query->setOption(
          'solr_param_bf',
          "recip(abs(ms(NOW,{$solr_field})),$m,{$a},{$b})"
        );
      }

      // Avoid the conversion into a lucene parser expression, keep edismax.
      $this->query->setOption(
        'solr_param_defType',
        "edismax"
      );

    }

  }

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();

    $options['fieldname']['default'] = 'created';
    $options['a_value']['default'] = '10';
    $options['b_value']['default'] = '0.1';
    $options['m_value']['default'] = '3.16e-11';
    $options['min_value']['default'] = '';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['fieldname'] = [
      '#type' => 'select',
      '#options' => $this->getDateFields(),
      '#title' => $this->t('Date fieldname'),
      '#default_value' => $this->options['fieldname'],
      '#description' => $this->t('First, a field must be added to the index with the type of "Date" to appear in this list. The following function query will be used: recip(abs(ms(NOW,datefield)),m,A,B). You can tweak the A, B, m parameters here.'),
      '#required' => TRUE,
    ];

    $form['a_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('A'),
      '#default_value' => $this->options['a_value'],
      '#required' => TRUE,
    ];

    $form['b_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('B'),
      '#default_value' => $this->options['b_value'],
      '#required' => TRUE,
    ];

    $form['m_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('m'),
      '#default_value' => $this->options['m_value'],
      '#required' => TRUE,
    ];

    $form['min_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum boost (optional)'),
      '#description' => $this->t("If set the calculated boost value for the field won't be lower than this."),
      '#default_value' => $this->options['min_value'],
    ];
  }

  /**
   * Retrieves a list of all available date fields.
   *
   * @return string[]
   *   An options list of date field identifiers mapped to their prefixed
   *   labels.
   */
  protected function getDateFields() {
    $fields = [];
    /** @var \Drupal\search_api\IndexInterface $index */
    $index = Index::load(substr($this->table, 17));

    $fields_info = $index->getFields();

    $dateFields = [];
    foreach ($fields_info as $key => $field) {
      if ($field->getType() === 'date') {
        $dateFields[] = $key;
      }
    }

    foreach ($dateFields as $field_id) {
      $fields[$field_id] = $fields_info[$field_id]->getPrefixedLabel();
    }

    return $fields;
  }

}
